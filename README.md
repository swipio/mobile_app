<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1> Swipio mobile </h1>
  <p>Mobile app for Android and iOS that helps you and your friends to choose film to watch together</p>
</div>



## 📝 About The Project
In a large group of friends, it is always difficult to choose a movie to watch that will suit everyone. This project aims to solve this problem. A group of friends individually scrolls through the list of available movies and uses swipe gestures to determine which movies they like. After that, the system gives out a list of films that can be watched, in order of rating (at the very top is the film that more friends liked)

[See full demo of project via this link](https://pollen-anger-934.notion.site/Swipio-App-Demo-f0ae21311fd344129b043fc5d6cc2369)

## Try Swipio!

[Download latest APK for Android](https://gitlab.com/swipio/mobile_app/-/jobs/artifacts/master/file/app.apk?job=build)


## ⚡️ Quick Start

1. Install Android Studio IDE or just use VS Code
2. [Install Dart and Flutter](https://flutter.dev/docs/get-started/install)
3. Clone project `git clone https://gitlab.com/swipio/mobile_app.git`
4. Open terminal in the directory of the project
5. `flutter pub get`
6. Download Android emulator [For VS Code](https://flutteragency.com/setup-emulator-for-vscode/)/[For Android Studio](https://developer.android.com/studio/run/managing-avds)
7. `flutter run`


## Mobile Application

Mobile application is written using crossplatform **Flutter** framework for **Dart** language. We used **BloC** Architecture to fit our nonfunctional requirements and make our system structurized, maintainable and testable.

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Gleb Osotov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>g.osotov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/glebosotov">@glebosotov</a> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>

  :boy: <b>Dmitriy Tsaplya</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>d.tsaplya@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://github.com/tsaplyadmitriy">@tsaplyadmitriy</a> <br>
</p>


## Github Stars

We see our application as a product, not as an open-source solution, but we have a small part of our project which may be an open-source library. We’ve developed our own file storage. It’s lightweight and is a good solution for small projects.

[Link to the file storage (26 stars now)](https://github.com/Markovvn1/swipio-file-storage)


## :scroll: License

`Swipio backend` is free and open-source software licensed under the [Apache 2.0 License](LICENSE)
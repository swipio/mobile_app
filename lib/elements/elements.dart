import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

import '/elements/textstyle.dart';

final elevatedButtonStyle = ElevatedButtonThemeData(
  style: ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
    ),
  ),
);

Widget swipioLogo() => Text('Swipio', style: TextStyles.logoBig);

ToastFuture swipioToast(BuildContext context, String message, IconData icon) {
  return showToastWidget(
      Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.red,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              onPressed: () {
                ToastManager().dismissAll(showAnim: true);
              },
              icon: Icon(
                icon,
                color: Colors.white,
              ),
            ),
            Text(
              message,
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ),
      context: context,
      position: StyledToastPosition.center,
      animation: StyledToastAnimation.fade,
      reverseAnimation: StyledToastAnimation.fade);
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '/elements/textstyle.dart';

class SwipioAppBar extends StatefulWidget implements PreferredSizeWidget {
  const SwipioAppBar({
    Key? key,
  })  : preferredSize = const Size.fromHeight(55.0),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _SwipioAppBarState createState() => _SwipioAppBarState();
}

class _SwipioAppBarState extends State<SwipioAppBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        height: 50,
        // decoration: BoxDecoration(
        //   color: Colors.white,
        //   border: Border(
        //     bottom: BorderSide(
        //       width: 1,
        //       color: Color(0xFFE5E5E5),
        //     ),
        //   ),
        // ),
        child: Center(
            child: Text('Swipio',
                style: TextStyles.logoBig.copyWith(fontSize: 30))),
      ),
    );
  }
}

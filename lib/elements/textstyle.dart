import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle get logoBig => const TextStyle(
      fontFamily: 'Alata',
      color: Colors.blue,
      letterSpacing: 0,
      fontWeight: FontWeight.w400,
      fontSize: 80);
}

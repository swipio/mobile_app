import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/bloc/auth/auth_bloc.dart';
import '/elements/elements.dart';
import '/network/models/token_response.dart';
import '/network/repository/auth_repository.dart';
import '../locator.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController emailController;
  late TextEditingController passwordController;
  late FocusNode emailFocus;
  late FocusNode passwordFocus;

  final _emailKey = GlobalKey<FormState>();
  final _passKey = GlobalKey<FormState>();

  @override
  initState() {
    emailController = TextEditingController(text: 'testemail2@mail.ru');
    passwordController = TextEditingController(text: 'password');
    emailFocus = FocusNode();
    passwordFocus = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    emailFocus.dispose();
    passwordFocus.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: BlocProvider(
        create: (_) => AuthBloc(locator<AuthenticationRepository>()),
        child: BlocConsumer<AuthBloc, AuthState>(listener: (context, state) {
          if (state is LoginFailure) {
            if (state.status == TokenState.wrongCredentials) {
              swipioToast(
                  context, 'Wrong credentials', Icons.error_outline_rounded);
              passwordController.text = '';
            }
          }
          if (state is LoginSuccess) {
            Navigator.of(context).pushReplacementNamed('/');
          }
        }, builder: (context, state) {
          return Stack(children: [
            state is LoadingState
                ? const Center(child: CircularProgressIndicator())
                : Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Spacer(flex: 2),
                      swipioLogo(),
                      const Text('Sign in',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold)),
                      const Spacer(flex: 3),
                      Form(
                        key: _emailKey,
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autocorrect: false,
                          controller: emailController,
                          focusNode: emailFocus,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            return RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value ?? '')
                                ? null
                                : 'Bad email format';
                          },
                          decoration: InputDecoration(
                            labelText: 'email',
                            border: InputBorder.none,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.secondary,
                                width: 1.5,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 16),
                      Form(
                        key: _passKey,
                        child: TextFormField(
                          autocorrect: false,
                          controller: passwordController,
                          focusNode: passwordFocus,
                          validator: (value) {
                            return value!.isNotEmpty ? null : 'Empty password';
                          },
                          decoration: InputDecoration(
                            labelText: 'password',
                            border: InputBorder.none,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.secondary,
                                width: 1.5,
                              ),
                            ),
                          ),
                          obscureText: true,
                          textInputAction: TextInputAction.done,
                        ),
                      ),
                      const Spacer(flex: 4),
                      ElevatedButton(
                        onPressed: () {
                          if (_emailKey.currentState!.validate() &&
                              _passKey.currentState!.validate()) {
                            BlocProvider.of<AuthBloc>(context).add(LoginEvent(
                                emailController.text, passwordController.text));
                          }
                        },
                        child: const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Text('Log in'),
                        ),
                      ),
                      TextButton(
                        child: const Text('Reset Password'),
                        onPressed: () {},
                      ),
                      TextButton(
                        child: const Text('Sign Up'),
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('/signup');
                        },
                      ),
                      const Spacer(flex: 1),
                    ],
                  )
          ]);
        }),
      ),
    ));
  }
}

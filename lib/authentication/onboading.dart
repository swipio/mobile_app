import 'package:flutter/material.dart';

import '/elements/elements.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(flex: 2, child: swipioLogo()),
          const Flexible(
            fit: FlexFit.loose,
            flex: 3,
            child: Text(
                'С последними правками правил App Store, Apple разрешит приложениям оставлять ссылки для оплаты подписки на внешнем сайте - изменения вступят в силу в начале 2022 года и коснутся подписных сервисов, типа Netflix и Spotify, а также приложений для чтения книг, просмотра журналов, газет, видео и прослушивания музыки'),
          ),
          Flexible(flex: 3, child: Image.asset('assets/images/banner.jpg')),
          Flexible(
            flex: 1,
            child: ElevatedButton(
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/signup');
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Text('Start'),
              ),
            ),
          )
        ],
      ),
    ));
  }
}

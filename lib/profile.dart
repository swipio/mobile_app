import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/bloc/group/group_bloc.dart';
import '/elements/elements.dart';
import '/locator.dart';
import 'authentication/login.dart';
import 'network/models/group_model.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  GroupResponse? _selection;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Spacer(flex: 2),
            swipioLogo(),
            const Text('Hi! Guy',
                style: TextStyle(fontSize: 32, fontWeight: FontWeight.w500)),
            const Spacer(flex: 3),
            SizedBox(
              height: 300,
              child: BlocProvider(
                create: (_) => locator.get<GroupBloc>(),
                child: BlocConsumer<GroupBloc, GroupState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is LoadingState || state is InitialState) {
                        return const Center(child: CircularProgressIndicator());
                      }
                      if (state is NoGroupState) {
                        return joinOrCreateGroup(
                            context, BlocProvider.of<GroupBloc>(context));
                      }
                      if (state is InGroupState) {
                        return SizedBox(
                            height: 300,
                            child: Column(
                              children: [
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextButton.icon(
                                          onPressed: () {
                                            createGroup(
                                                context,
                                                BlocProvider.of<GroupBloc>(
                                                    context));
                                          },
                                          icon: const Icon(Icons.add_rounded),
                                          label: const Text('Create a group')),
                                      ElevatedButton.icon(
                                          onPressed: () {
                                            joinGroup(
                                                context,
                                                BlocProvider.of<GroupBloc>(
                                                    context));
                                          },
                                          icon: const Icon(
                                              Icons.group_add_rounded),
                                          label: const Text('Join a group')),
                                      PopupMenuButton<GroupResponse>(
                                          icon: const Icon(
                                              Icons.list_alt_rounded),
                                          onSelected: (GroupResponse group) {
                                            setState(() {
                                              _selection = group;
                                            });
                                          },
                                          itemBuilder: (context) {
                                            return state.groups
                                                .map((group) => PopupMenuItem<
                                                        GroupResponse>(
                                                    value: group,
                                                    child:
                                                        Text(group.group.name)))
                                                .toList();
                                          }),
                                    ],
                                  ),
                                ),
                                _groupInfo(
                                    context, _selection ?? state.groups.first),
                              ],
                            ));
                      }
                      throw Exception('Unreacheable state');
                    }),
              ),
            ),
            const Spacer(flex: 3),
            TextButton(
                onPressed: () {
                  _logout();
                },
                child: const Text('Log out')),
            const Spacer(flex: 1),
          ],
        ),
      ),
    );
  }

  Widget _groupInfo(BuildContext context, GroupResponse groupContainer) {
    return Container(
      height: 200,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.people,
                  size: 80, color: Theme.of(context).colorScheme.secondary),
              const SizedBox(width: 20),
              Expanded(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        groupContainer.group.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      Text(
                        groupContainer.group.description,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ]),
              ),
            ],
          ),
          const Spacer(flex: 1),
          const Text(
              'Invite your friends using the button below, or the Group Code',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          const Spacer(flex: 1),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 64,
                width: 64,
                alignment: Alignment.center,
                // padding: const EdgeInsets.all(16),
                decoration: const BoxDecoration(
                  color: Colors.teal,
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
                child: Text(
                  groupContainer.group.groupId.toString(),
                  maxLines: 1,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(width: 20),
              Ink(
                height: 64,
                width: 64,
                decoration: const ShapeDecoration(
                  color: CupertinoColors.systemIndigo,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      Clipboard.setData(ClipboardData(
                          text: groupContainer.group.groupId.toString()));
                      swipioToast(
                          context, 'Group ID copied to clipboard.', Icons.copy);
                    },
                    icon: const Icon(Icons.copy, color: Colors.white)),
              ),
              const SizedBox(width: 20),
              Ink(
                height: 64,
                width: 64,
                decoration: const ShapeDecoration(
                  color: CupertinoColors.systemOrange,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      Share.share(
                          'Join my group on swipio with this code ${groupContainer.group.groupId}');
                    },
                    icon: const Icon(Icons.share, color: Colors.white)),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _logout() async {
    const storage = FlutterSecureStorage();
    await storage.deleteAll();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('authState', 'loggedOut');
    prefs.remove('groupId');
    pushNewScreen(
      context,
      screen: const LoginScreen(),
      withNavBar: false, // OPTIONAL VALUE. True by default.
      pageTransitionAnimation: PageTransitionAnimation.cupertino,
    );
  }
}

Column joinOrCreateGroup(BuildContext context, GroupBloc bloc) {
  return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
    TextButton.icon(
        onPressed: () {
          createGroup(context, bloc);
        },
        icon: const Icon(Icons.add_rounded),
        label: const Text('Create a group')),
    ElevatedButton.icon(
        onPressed: () {
          joinGroup(context, bloc);
        },
        icon: const Icon(Icons.group_add_rounded),
        label: const Text('Join a group')),
  ]);
}

createGroup(BuildContext context, GroupBloc bloc) {
  final _nameKey = GlobalKey<FormState>();
  final _descriptionKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _nameFocus = FocusNode();
  final _descriptionFocus = FocusNode();
  showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          insetPadding: const EdgeInsets.symmetric(horizontal: 8),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            height: 320,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Spacer(flex: 1),
                const Text('Create a group',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
                const Spacer(flex: 4),
                Form(
                  key: _nameKey,
                  child: TextFormField(
                    autocorrect: false,
                    controller: _nameController,
                    focusNode: _nameFocus,
                    validator: (value) {
                      return value!.isNotEmpty ? null : 'Empty name';
                    },
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      labelText: 'Name',
                      border: InputBorder.none,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(
                          color: Colors.grey,
                          width: 1.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          color: Theme.of(context).colorScheme.secondary,
                          width: 1.5,
                        ),
                      ),
                    ),
                  ),
                ),
                const Spacer(flex: 1),
                Form(
                  key: _descriptionKey,
                  child: TextFormField(
                    autocorrect: false,
                    controller: _descriptionController,
                    focusNode: _descriptionFocus,
                    validator: (value) {
                      return value!.isNotEmpty ? null : 'Empty description';
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      labelText: 'Description',
                      border: InputBorder.none,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(
                          color: Colors.grey,
                          width: 1.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          color: Theme.of(context).colorScheme.secondary,
                          width: 1.5,
                        ),
                      ),
                    ),
                  ),
                ),
                const Spacer(flex: 3),
                ElevatedButton.icon(
                    onPressed: () {
                      if (_nameKey.currentState!.validate() &&
                          _descriptionKey.currentState!.validate()) {
                        Navigator.of(context).pop();
                        bloc.add(CreateGroup(
                            _nameController.text, _descriptionController.text));
                      }
                    },
                    icon: const Icon(Icons.done),
                    label: const Text('Create')),
                const Spacer(flex: 1),
              ],
            ),
          ),
        );
      });
}

joinGroup(BuildContext context, GroupBloc bloc) {
  final _codeKey = GlobalKey<FormState>();

  final _codeController = TextEditingController();

  final _codeFocus = FocusNode();

  showCupertinoModalPopup(
      context: context,
      builder: (context) {
        return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            insetPadding: const EdgeInsets.symmetric(horizontal: 8),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              height: 320,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const Spacer(flex: 1),
                  const Text('Join a group',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
                  const Spacer(flex: 4),
                  Form(
                    key: _codeKey,
                    child: TextFormField(
                      autocorrect: false,
                      controller: _codeController,
                      focusNode: _codeFocus,
                      validator: (value) {
                        return RegExp(r'^\d+').hasMatch(value ?? '')
                            ? null
                            : 'Empty code';
                      },
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: 'Code',
                        border: InputBorder.none,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                            width: 1.0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Theme.of(context).colorScheme.secondary,
                            width: 1.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Spacer(flex: 3),
                  ElevatedButton.icon(
                      onPressed: () {
                        if (_codeKey.currentState!.validate()) {
                          Navigator.of(context).pop();
                          bloc.add(JoinGroup(int.parse(_codeController.text)));
                        }
                      },
                      icon: const Icon(Icons.group_add_rounded),
                      label: const Text('Join')),
                  const Spacer(flex: 1),
                ],
              ),
            ));
      });
}

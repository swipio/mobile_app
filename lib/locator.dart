import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import '/bloc/film_suggestion/film_suggestion_bloc.dart';
import '/bloc/group/group_bloc.dart';
import '/bloc/swipe/swipe_bloc.dart';
import '/network/api/auth_api.dart';
import '/network/api/group_api.dart';
import '/network/repository/auth_repository.dart';
import '/network/repository/group_repository.dart';

final GetIt locator = GetIt.instance;

Future initLocator() async {
  Dio dio = Dio(BaseOptions(
    connectTimeout: 1000 * 5,
    receiveTimeout: 1000 * 5,
    sendTimeout: 1000 * 5,
  ));

  locator.registerSingleton(dio);

  locator.registerLazySingleton<AuthenticationApi>(
      () => AuthenticationApi(locator.get<Dio>()));
  locator.registerLazySingleton<AuthenticationRepository>(
      () => AuthenticationRepository(locator.get<AuthenticationApi>()));

  locator.registerLazySingleton<GroupApi>(() => GroupApi(locator.get<Dio>()));
  locator.registerLazySingleton<GroupRepository>(
      () => GroupRepository(locator.get<GroupApi>()));

  locator.registerLazySingleton<FilmSuggestionBloc>(
      () => FilmSuggestionBloc(locator.get<GroupRepository>()));

  locator.registerLazySingleton<GroupBloc>(
      () => GroupBloc(locator.get<GroupRepository>()));

  locator.registerLazySingleton<SwipeBloc>(
      () => SwipeBloc(locator.get<GroupRepository>()));
}

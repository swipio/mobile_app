import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/locator.dart';
import '/network/models/film_models.dart';
import '/network/repository/auth_repository.dart';
import '/network/repository/group_repository.dart';

part 'film_suggestion_event.dart';
part 'film_suggestion_state.dart';

class FilmSuggestionBloc
    extends Bloc<FilmSuggestionEvent, FilmsSuggestionState> {
  FilmSuggestionBloc(this.repository) : super(InitialState()) {
    add(GetStatus());
    authRepository = locator<AuthenticationRepository>();
  }

  final GroupRepository repository;
  late AuthenticationRepository authRepository;

  @override
  Stream<FilmsSuggestionState> mapEventToState(
      FilmSuggestionEvent event) async* {
    final token = await authRepository.getSecureToken();
    if (event is GetStatus) {
      final prefs = await SharedPreferences.getInstance();
      if (prefs.containsKey('groupId')) {
        final ids = prefs.getStringList('groupId')!;
        add(GetFilmsToWatchEvent(ids.map((e) => int.parse(e)).toList()));
        yield LoadingState();
      } else {
        yield NoGroupState();
      }
    }
    if (event is GetFilmsToWatchEvent) {
      Map<String, List<FilmContainer>> films = {};
      await Future.wait(
        event.groupIds.map((id) => filmsAndGroup(id, token)),
      ).then((entries) {
        for (var e in entries) {
          films[e.first] = e[1];
        }
      });
      yield FilmsReady(films);
    }
  }

  Future<List<dynamic>> filmsAndGroup(int id, String token) async {
    final films = await repository.topFilms(token, id);
    final String groupName = (await repository.getGroup(token, id)).group.name;
    return [groupName, films.films];
  }
}

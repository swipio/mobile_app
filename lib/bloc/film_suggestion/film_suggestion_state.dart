part of 'film_suggestion_bloc.dart';

abstract class FilmsSuggestionState {}

class InitialState extends FilmsSuggestionState {}

class LoadingState extends FilmsSuggestionState {}

class NoGroupState extends FilmsSuggestionState {}

class FilmsReady extends FilmsSuggestionState {
  final Map<String, List<FilmContainer>> films;

  FilmsReady(this.films);
}

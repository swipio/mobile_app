import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/bloc/film_suggestion/film_suggestion_bloc.dart' as suggestion;
import '/bloc/swipe/swipe_bloc.dart' as swipe;
import '/locator.dart';
import '/network/models/group_model.dart';
import '/network/repository/auth_repository.dart';
import '/network/repository/group_repository.dart';

part 'group_event.dart';
part 'group_state.dart';

class GroupBloc extends Bloc<GroupEvent, GroupState> {
  GroupBloc(this.repository) : super(InitialState()) {
    add(GetStatus());
    authRepository = locator<AuthenticationRepository>();
  }

  final GroupRepository repository;
  late AuthenticationRepository authRepository;

  @override
  Stream<GroupState> mapEventToState(GroupEvent event) async* {
    final token = await authRepository.getSecureToken();
    if (event is GetStatus) {
      final prefs = await SharedPreferences.getInstance();
      if (prefs.containsKey('groupId')) {
        add(GetGroups(prefs
            .getStringList('groupId')!
            .map((element) => int.parse(element))
            .toSet()
            .toList()));
      } else {
        yield NoGroupState();
      }
    }
    if (event is GetGroups) {
      yield LoadingState();
      List<GroupResponse> groups = [];
      await Future.wait(
              (event.groupIds.map((id) => repository.getGroup(token, id))))
          .then((value) {
        groups = value;
      });
      yield InGroupState(groups);
    }
    if (event is CreateGroup) {
      yield LoadingState();
      final groupResponse =
          await repository.createGroup(token, event.name, event.description);
      setGroupId(groupResponse.group.groupId);
      final prefs = await SharedPreferences.getInstance();
      add(GetGroups(prefs
          .getStringList('groupId')!
          .map((element) => int.parse(element))
          .toSet()
          .toList()));
    }
    if (event is JoinGroup) {
      yield LoadingState();
      final _ = await repository.joinGroup(token, event.groupId);
      // TODO: check group's existance
      setGroupId(event.groupId);
      final prefs = await SharedPreferences.getInstance();
      add(GetGroups(prefs
          .getStringList('groupId')!
          .map((element) => int.parse(element))
          .toSet()
          .toList()));
    }
  }

  setGroupId(int id) async {
    final prefs = await SharedPreferences.getInstance();
    List<String> existingIds =
        prefs.containsKey('groupId') ? prefs.getStringList('groupId')! : [];
    existingIds.add(id.toString());
    prefs.setStringList('groupId', existingIds);
    locator.get<suggestion.FilmSuggestionBloc>().add(suggestion.GetStatus());
    locator.get<swipe.SwipeBloc>().add(swipe.GetStatus());
  }
}

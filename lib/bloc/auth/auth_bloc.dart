import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/bloc/film_suggestion/film_suggestion_bloc.dart' as suggestion;
import '/bloc/group/group_bloc.dart' as group;
import '/bloc/swipe/swipe_bloc.dart' as swipe;
import '/network/models/registration_response.dart';
import '/network/models/token_response.dart';
import '/network/repository/auth_repository.dart';
import '../../locator.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(this.repository) : super(InitialState());
  final AuthenticationRepository repository;

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is RegisterUserEvent) {
      yield LoadingState();
      final response = await repository.registerUser(
          event.username, event.password, event.firstName, event.lastName);
      if (response == RegistrationStatus.success) {
        add(LoginEvent(event.username, event.password));
      } else if (response == RegistrationStatus.emailTaken) {
        yield RegistrationFailure(response);
      } else {
        throw Exception(response);
      }
    }
    if (event is LoginEvent) {
      yield LoadingState();
      final response =
          await repository.getToken(event.username, event.password);
      if (response.state == TokenState.success) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('authState', 'loggedIn');
        repository.setSecureToken(response.accessToken);
        locator<group.GroupBloc>().add(group.GetStatus());
        locator<suggestion.FilmSuggestionBloc>().add(suggestion.GetStatus());
        locator<swipe.SwipeBloc>().add(swipe.GetStatus());
        yield LoginSuccess();
        yield RegistrationSuccess(response.tokenType);
      } else if (response.state == TokenState.wrongCredentials) {
        yield LoginFailure(response.state);
      } else {
        throw Exception(response.tokenType);
      }
    }
  }
}

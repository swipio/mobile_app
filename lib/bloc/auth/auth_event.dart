part of 'auth_bloc.dart';

abstract class AuthEvent {}

class RegisterUserEvent extends AuthEvent {
  final String username;
  final String password;
  final String firstName;
  final String lastName;

  RegisterUserEvent(
      this.username, this.password, this.firstName, this.lastName);
}

class LoginEvent extends AuthEvent {
  final String username;
  final String password;

  LoginEvent(this.username, this.password);
}

class LogoutEvent extends AuthEvent {}

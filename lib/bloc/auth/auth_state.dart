part of 'auth_bloc.dart';

abstract class AuthState {}

class InitialState extends AuthState {}

class LoadingState extends AuthState {}

class RegistrationSuccess extends AuthState {
  final String status;

  RegistrationSuccess(this.status);
}

class RegistrationFailure extends AuthState {
  final RegistrationStatus status;

  RegistrationFailure(this.status);
}

class LoginSuccess extends AuthState {}

class LoginFailure extends AuthState {
  final TokenState status;

  LoginFailure(this.status);
}

class LogoutSuccess extends AuthState {}

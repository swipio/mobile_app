import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/bloc/film_suggestion/film_suggestion_bloc.dart' as suggestion;
import '/network/models/film_models.dart';
import '/network/repository/auth_repository.dart';
import '/network/repository/group_repository.dart';
import '../../locator.dart';

part 'swipe_event.dart';
part 'swipe_state.dart';

class SwipeBloc extends Bloc<SwipeEvent, SwipeState> {
  SwipeBloc(this.repository) : super(InitialState()) {
    authRepository = locator<AuthenticationRepository>();

    on<GetStatus>(_getStatus);
    on<GetSwipes>(_getSwipes);
    on<GetMoreSwipes>(_getMoreSwipes);
    on<SwipeAction>(_swipeAction);

    add(GetStatus());
  }

  final GroupRepository repository;
  late AuthenticationRepository authRepository;
  static const int amount = 20;

  _getStatus(GetStatus event, Emitter<SwipeState> emit) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('groupId')) {
      add(GetSwipes(prefs
          .getStringList('groupId')!
          .map((element) => int.parse(element))
          .toSet()
          .toList()));
    } else {
      emit(NoGroupState());
    }
  }

  _getSwipes(GetSwipes event, Emitter<SwipeState> emit) async {
    final token = await authRepository.getSecureToken();
    emit(LoadingState());
    Map<int, FilmsForGroup> swipeFilms = {};
    await Future.wait(
      event.groupIds.map((id) => filmsAndGroup(id, token)),
    ).then((entries) {
      for (var e in entries) {
        swipeFilms[e.id] = e;
      }
    });
    emit(UpdatedSwipe(swipeFilms));
  }

  Future<FilmsForGroup> filmsAndGroup(int id, String token) async {
    final FilmsForGroup films =
        await repository.filmsToView(token, id, SwipeBloc.amount);
    final String groupName = (await repository.getGroup(token, id)).group.name;
    return FilmsForGroup(films.id, films.films, groupName: groupName);
  }

  _getMoreSwipes(GetMoreSwipes event, Emitter<SwipeState> emit) async {
    final token = await authRepository.getSecureToken();
    final newFilms =
        await repository.filmsToView(token, event.updateId, SwipeBloc.amount);
    var map = event.swipeFilms;
    var oldFilms = map[event.updateId]!.films;
    oldFilms.addAll(newFilms.films);
    final combinedFilms = oldFilms.toSet().toList();
    map[event.updateId] = FilmsForGroup(event.updateId, combinedFilms,
        groupName: map[event.updateId]!.groupName);
    emit(UpdatedSwipe(map));
  }

  _swipeAction(SwipeAction event, Emitter<SwipeState> emit) async {
    final token = await authRepository.getSecureToken();
    repository.markFilm(token, event.filmId, event.type);
    var map = event.swipeFilmsBeforeSwipe;
    map[event.groupId]!
        .films
        .removeWhere((element) => element.film.filmId == event.filmId);
    // TODO: check and maybe load more films
    locator<suggestion.FilmSuggestionBloc>().add(suggestion.GetStatus());
    // emit(UpdatedSwipe(map));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:swipe_cards/swipe_cards.dart';

import '/bloc/group/group_bloc.dart' as group;
import '/bloc/swipe/swipe_bloc.dart';
import '/network/models/film_models.dart';
import 'elements/appbar.dart';
import 'elements/modal_movie.dart';
import 'elements/textstyle.dart';
import 'locator.dart';
import 'profile.dart';

class SwipeCardsScreen extends StatefulWidget {
  const SwipeCardsScreen({Key? key}) : super(key: key);

  @override
  _SwipeCardsScreenState createState() => _SwipeCardsScreenState();
}

class _SwipeCardsScreenState extends State<SwipeCardsScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => locator<SwipeBloc>(),
      child: BlocConsumer<SwipeBloc, SwipeState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is InitialState || state is LoadingState) {
              return const Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if (state is NoGroupState) {
              return Scaffold(
                  appBar: const SwipioAppBar(),
                  body: Center(
                      child: joinOrCreateGroup(
                          context, locator.get<group.GroupBloc>())));
            }
            if (state is UpdatedSwipe) {
              return DefaultTabController(
                length: state.swipeFilms.length,
                child: Scaffold(
                    appBar: AppBar(
                      backgroundColor: Theme.of(context).canvasColor,
                      title: Text('Swipio',
                          style: TextStyles.logoBig.copyWith(fontSize: 30)),
                      bottom: TabBar(
                          isScrollable: true,
                          labelColor: Theme.of(context).iconTheme.color,
                          tabs: state.swipeFilms.values
                              .map((value) => Tab(text: value.groupName))
                              .toList()),
                      leading: Row(
                        children: [
                          const SizedBox(width: 8),
                          Icon(Icons.arrow_back_ios,
                              color: Theme.of(context).hintColor),
                          Icon(Icons.thumb_down_rounded,
                              color: Theme.of(context).hintColor)
                        ],
                      ),
                      actions: [
                        Row(
                          children: [
                            Icon(Icons.thumb_up_rounded,
                                color: Theme.of(context).hintColor),
                            const SizedBox(width: 4),
                            Icon(Icons.arrow_forward_ios,
                                color: Theme.of(context).hintColor),
                            const SizedBox(width: 8),
                          ],
                        )
                      ],
                    ),
                    body: TabBarView(
                      children: state.swipeFilms.values
                          .map((element) => Center(
                                child: _swipeStack(
                                    element,
                                    BlocProvider.of<SwipeBloc>(context),
                                    state.swipeFilms),
                              ))
                          .toList(),
                    )),
              );
            }
            throw Exception('Unreacheable state');
          }),
    );
  }

  Widget _swipeStack(FilmsForGroup filmsForGroup, SwipeBloc bloc,
      Map<int, FilmsForGroup> swipeFilms) {
    var items = filmsForGroup.films
        .map((film) => SwipeItem(
            content: film,
            likeAction: () {
              bloc.add(SwipeAction(film.film.filmId, SwipeType.left,
                  filmsForGroup.id, swipeFilms));
            },
            nopeAction: () {
              bloc.add(SwipeAction(film.film.filmId, SwipeType.right,
                  filmsForGroup.id, swipeFilms));
            }))
        .toList();
    var _matchEngine = MatchEngine(swipeItems: items);
    return items.isNotEmpty
        ? SwipeCards(
            matchEngine: _matchEngine,
            onStackFinished: () {
              bloc.add(GetMoreSwipes(swipeFilms, filmsForGroup.id));
            },
            itemBuilder: (context, index) {
              return Card(
                child: Container(
                    padding: const EdgeInsets.all(16),
                    width: double.infinity,
                    height: 500,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Hero(
                          tag: items[index].content.film.filmId,
                          child: SizedBox(
                            width: 240,
                            height: 240,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(16.0),
                              child: Image.network(
                                  items[index].content.film.image,
                                  fit: BoxFit.fitWidth),
                            ),
                          ),
                        ),
                        Text(items[index].content.film.title,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 24,
                                letterSpacing: -1)),
                        Text(items[index].content.film.description,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16)),
                        ElevatedButton.icon(
                            onPressed: () {
                              pushDynamicScreen(
                                context,
                                screen: CupertinoPageRoute(
                                  builder: (BuildContext context) {
                                    return MovieModal(
                                        film: items[index].content);
                                  },
                                ),
                                withNavBar: true,
                              );
                            },
                            icon: const Icon(Icons.info),
                            label: const Text('Read more'))
                      ],
                    )),
                margin: const EdgeInsets.all(8),
                elevation: 8,
                shape: RoundedRectangleBorder(
                  // side: BorderSide(
                  //   color: Theme.of(context).colorScheme.secondary,
                  //   width: 1.5,
                  // ),
                  borderRadius: BorderRadius.circular(16.0),
                ),
              );
            })
        : const Center(
            child: Text('No films to Swipe right now',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          );
  }
}

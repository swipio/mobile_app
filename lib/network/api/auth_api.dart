import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '/network/base_url.dart';
import '/network/models/registration_response.dart';
import '/network/models/token_response.dart';
import '/network/models/user_model.dart';

part 'auth_api.g.dart';

@RestApi(baseUrl: BaseUrls.baseUrl)
abstract class AuthenticationApi {
  factory AuthenticationApi(Dio dio) = _AuthenticationApi;

  @POST('/token')
  @FormUrlEncoded()
  Future<TokenResponse> getToken(
      @Field() String? username, @Field() String? password);

  @POST('/api/v1/users/register')
  Future<RegistrationResponse> registerUser(
      @Query('username') String? username,
      @Query('password') String? password,
      @Query('first_name') String? firstName,
      @Query('last_name') String? lastName);

  @GET('/api/v1/users/me')
  Future<UserModel> getMyUserInfo(@Header('Authorization') String? token);

  @GET('/api/v1/users/{user_id}')
  Future<UserModel> getUserInfo(
      @Header('Authorization') String? token, @Path('user_id') String userId);
}

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '/network/api/auth_api.dart';
import '/network/models/registration_response.dart';
import '/network/models/token_response.dart';
import '/network/models/user_model.dart';

class AuthenticationRepository {
  final AuthenticationApi _api;
  AuthenticationRepository(this._api);

  Future<TokenResponse> getToken(String username, String password) async {
    try {
      return await _api.getToken(username, password);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! == 401) {
          return TokenResponse('', err.response!.statusMessage!,
              state: TokenState.wrongCredentials);
        }
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<RegistrationStatus> registerUser(String username, String password,
      String firstName, String lastName) async {
    try {
      final registrationResponse =
          await _api.registerUser(username, password, firstName, lastName);
      return registrationResponse.state;
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! == 409) {
          return RegistrationStatus.emailTaken;
        }
      }
      throw Exception();
    }
  }

  Future<UserModel> getUserInfo(String token, String userId) async {
    try {
      return await _api.getUserInfo(token, userId);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<String> getSecureToken() async {
    const storage = FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    if (token != null) {
      return token;
    } else {
      throw Exception('Token is not available');
    }
  }

  setSecureToken(String token) async {
    const storage = FlutterSecureStorage();
    storage.write(key: 'token', value: 'Bearer $token');
  }
}

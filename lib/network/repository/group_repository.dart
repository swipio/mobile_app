import 'dart:async';

import 'package:dio/dio.dart';

import '/bloc/swipe/swipe_bloc.dart';
import '/network/api/group_api.dart';
import '/network/models/film_models.dart';
import '/network/models/group_model.dart';

class GroupRepository {
  final GroupApi _api;
  GroupRepository(this._api);

  Future<GroupResponse> createGroup(
      String token, String name, String description) async {
    try {
      return await _api.createGroup(token, name, description);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<GroupResponse> getGroup(String token, int groupId) async {
    try {
      return await _api.getGroup(token, groupId);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<String> joinGroup(String token, int groupId) async {
    try {
      return await _api.joinGroup(token, groupId);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<FilmsForGroup> topFilms(String token, int groupId) async {
    try {
      final response = await _api.topFilms(token, groupId);
      return FilmsForGroup(groupId, response.films);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<FilmsForGroup> filmsToView(
      String token, int groupId, int count) async {
    try {
      final response = await _api.filmsToView(token, groupId, count);
      return FilmsForGroup(groupId, response.films);
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }

  Future<String> markFilm(String token, int filmId, SwipeType type) async {
    try {
      return await _api.markFilm(
          token, filmId, type == SwipeType.left ? 'like' : 'dont_like');
    } catch (err) {
      if (err is DioError) {
        if (err.response!.statusCode! >= 500) {
          throw Exception();
        }
      }
      throw Exception();
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupResponse _$GroupResponseFromJson(Map<String, dynamic> json) =>
    GroupResponse(
      GroupModel.fromJson(json['group'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GroupResponseToJson(GroupResponse instance) =>
    <String, dynamic>{
      'group': instance.group,
    };

GroupModel _$GroupModelFromJson(Map<String, dynamic> json) => GroupModel(
      json['created_at'] as String,
      json['updated_at'] as String,
      json['group_id'] as int,
      json['name'] as String,
      json['description'] as String,
      (json['users'] as List<dynamic>)
          .map((e) => UserModelContainer.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GroupModelToJson(GroupModel instance) =>
    <String, dynamic>{
      'created_at': instance.created,
      'updated_at': instance.updated,
      'group_id': instance.groupId,
      'name': instance.name,
      'description': instance.description,
      'users': instance.users,
    };

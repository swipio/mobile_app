// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModelContainer _$UserModelContainerFromJson(Map<String, dynamic> json) =>
    UserModelContainer(
      UserModel.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserModelContainerToJson(UserModelContainer instance) =>
    <String, dynamic>{
      'user': instance.user,
    };

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      json['login'] as String,
      json['first_name'] as String,
      json['last_name'] as String,
      json['created_at'] as String,
      json['updated_at'] as String,
      json['is_admin'] as bool,
      json['disabled'] as bool,
      json['user_id'] as int,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'login': instance.login,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'created_at': instance.registrationDate,
      'updated_at': instance.lastEditDate,
      'is_admin': instance.admin,
      'disabled': instance.disabled,
      'user_id': instance.userId,
    };

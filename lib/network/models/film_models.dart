import 'package:json_annotation/json_annotation.dart';

part 'film_models.g.dart';

@JsonSerializable()
class FilmsResponse {
  final List<FilmContainer> films;

  FilmsResponse(this.films);

  factory FilmsResponse.fromJson(Map<String, dynamic> json) =>
      _$FilmsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FilmsResponseToJson(this);
}

@JsonSerializable()
class FilmContainer {
  final Film film;
  final FilmStat stat;

  FilmContainer(this.film, this.stat);
  factory FilmContainer.fromJson(Map<String, dynamic> json) =>
      _$FilmContainerFromJson(json);

  Map<String, dynamic> toJson() => _$FilmContainerToJson(this);
}

class FilmsForGroup {
  final int id;
  final List<FilmContainer> films;
  final String? groupName;

  FilmsForGroup(this.id, this.films, {this.groupName});
}

@JsonSerializable()
class Film {
  @JsonKey(name: 'film_id')
  final int filmId;
  final String title;
  final String description;
  @JsonKey(name: 'release_year')
  final int releaseYear;
  final int duration;
  @JsonKey(name: 'preview_image_url')
  final String image;

  Film(this.filmId, this.title, this.description, this.releaseYear,
      this.duration, this.image);
  factory Film.fromJson(Map<String, dynamic> json) => _$FilmFromJson(json);
  Map<String, dynamic> toJson() => _$FilmToJson(this);
}

@JsonSerializable()
class FilmStat {
  @JsonKey(name: 'for')
  final int inFavor;
  final int against;
  final int abstained;

  FilmStat(this.inFavor, this.against, this.abstained);
  factory FilmStat.fromJson(Map<String, dynamic> json) =>
      _$FilmStatFromJson(json);
  Map<String, dynamic> toJson() => _$FilmStatToJson(this);
}

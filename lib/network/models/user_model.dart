import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModelContainer {
  final UserModel user;

  UserModelContainer(this.user);

  factory UserModelContainer.fromJson(Map<String, dynamic> json) =>
      _$UserModelContainerFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelContainerToJson(this);
}

@JsonSerializable()
class UserModel {
  @JsonKey(name: 'login')
  final String login;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'created_at')
  final String registrationDate;
  @JsonKey(name: 'updated_at')
  final String lastEditDate;
  @JsonKey(name: 'is_admin')
  final bool admin;
  final bool disabled;
  @JsonKey(name: 'user_id')
  final int userId;

  UserModel(this.login, this.firstName, this.lastName, this.registrationDate,
      this.lastEditDate, this.admin, this.disabled, this.userId);

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}

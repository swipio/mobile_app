// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilmsResponse _$FilmsResponseFromJson(Map<String, dynamic> json) =>
    FilmsResponse(
      (json['films'] as List<dynamic>)
          .map((e) => FilmContainer.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FilmsResponseToJson(FilmsResponse instance) =>
    <String, dynamic>{
      'films': instance.films,
    };

FilmContainer _$FilmContainerFromJson(Map<String, dynamic> json) =>
    FilmContainer(
      Film.fromJson(json['film'] as Map<String, dynamic>),
      FilmStat.fromJson(json['stat'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FilmContainerToJson(FilmContainer instance) =>
    <String, dynamic>{
      'film': instance.film,
      'stat': instance.stat,
    };

Film _$FilmFromJson(Map<String, dynamic> json) => Film(
      json['film_id'] as int,
      json['title'] as String,
      json['description'] as String,
      json['release_year'] as int,
      json['duration'] as int,
      json['preview_image_url'] as String,
    );

Map<String, dynamic> _$FilmToJson(Film instance) => <String, dynamic>{
      'film_id': instance.filmId,
      'title': instance.title,
      'description': instance.description,
      'release_year': instance.releaseYear,
      'duration': instance.duration,
      'preview_image_url': instance.image,
    };

FilmStat _$FilmStatFromJson(Map<String, dynamic> json) => FilmStat(
      json['for'] as int,
      json['against'] as int,
      json['abstained'] as int,
    );

Map<String, dynamic> _$FilmStatToJson(FilmStat instance) => <String, dynamic>{
      'for': instance.inFavor,
      'against': instance.against,
      'abstained': instance.abstained,
    };

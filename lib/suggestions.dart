import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '/bloc/group/group_bloc.dart' as group;
import '/locator.dart';
import '/network/models/film_models.dart';
import '/profile.dart';
import 'bloc/film_suggestion/film_suggestion_bloc.dart';
import 'elements/appbar.dart';
import 'elements/modal_movie.dart';
import 'elements/textstyle.dart';

class SuggestionsScreen extends StatefulWidget {
  const SuggestionsScreen({Key? key}) : super(key: key);

  @override
  _SuggestionsScreenState createState() => _SuggestionsScreenState();
}

class _SuggestionsScreenState extends State<SuggestionsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => locator.get<FilmSuggestionBloc>(),
      child: BlocConsumer<FilmSuggestionBloc, FilmsSuggestionState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is LoadingState || state is InitialState) {
              return const Scaffold(
                  appBar: SwipioAppBar(),
                  body: Center(child: CircularProgressIndicator()));
            }
            if (state is NoGroupState) {
              return Scaffold(
                  appBar: const SwipioAppBar(),
                  body: Center(
                      child: joinOrCreateGroup(
                          context, locator.get<group.GroupBloc>())));
            }
            if (state is FilmsReady) {
              return DefaultTabController(
                length: state.films.length,
                child: Scaffold(
                    appBar: AppBar(
                      backgroundColor: Theme.of(context).canvasColor,
                      title: Text('Swipio',
                          style: TextStyles.logoBig.copyWith(fontSize: 30)),
                      bottom: TabBar(
                          isScrollable: true,
                          labelColor: Theme.of(context).iconTheme.color,
                          tabs: state.films.keys
                              .map((key) => Tab(text: key))
                              .toList()),
                    ),
                    body: TabBarView(
                      children: state.films.values
                          .map((element) => filmsList(element))
                          .toList(),
                    )),
              );
            }
            throw Exception('Unreacheable state');
          }),
    );
  }

  Widget filmsList(List<FilmContainer> films) {
    return ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        itemCount: films.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              pushDynamicScreen(
                context,
                screen: CupertinoPageRoute(
                  builder: (BuildContext context) {
                    return MovieModal(film: films[index]);
                  },
                ),
                withNavBar: true,
              );
            },
            child: FilmCard(filmContainer: films[index]),
          );
        });
  }
}

class FilmCard extends StatelessWidget {
  final FilmContainer filmContainer;
  const FilmCard({
    Key? key,
    required this.filmContainer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
          padding: const EdgeInsets.all(16),
          width: double.infinity,
          height: 180,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 120,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Hero(
                      tag: filmContainer.film.filmId,
                      child: SizedBox(
                        width: 120,
                        height: 120,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(16.0),
                          child: Image.network(filmContainer.film.image,
                              fit: BoxFit.fitWidth),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(filmContainer.film.title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              letterSpacing: -1)),
                      SizedBox(
                          width: 170,
                          child: RatingBar(filmContainer: filmContainer))
                      // Text(filmContainer.film.description,
                      //     maxLines: 4,
                      //     overflow: TextOverflow.ellipsis,
                      //     style: TextStyle(
                      //         fontWeight: FontWeight.w500, fontSize: 16)),
                    ]),
              )
            ],
          )),
      margin: const EdgeInsets.all(8),
      elevation: 5,
      shape: RoundedRectangleBorder(
        // side: BorderSide(
        //   color: Theme.of(context).colorScheme.secondary,
        //   width: 1.5,
        // ),
        borderRadius: BorderRadius.circular(16.0),
      ),
    );
  }
}

class RatingBar extends StatelessWidget {
  const RatingBar({
    Key? key,
    required this.filmContainer,
  }) : super(key: key);

  final FilmContainer filmContainer;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Row(
          children: [
            const Icon(Icons.thumb_up_rounded,
                color: CupertinoColors.systemGreen),
            const SizedBox(width: 3),
            Text(filmContainer.stat.inFavor.toString()),
          ],
        ),
        Row(
          children: [
            const Icon(Icons.help_outline_outlined),
            const SizedBox(width: 3),
            Text(filmContainer.stat.abstained.toString()),
          ],
        ),
        Row(
          children: [
            const Icon(Icons.thumb_down_rounded,
                color: CupertinoColors.systemRed),
            const SizedBox(width: 3),
            Text(filmContainer.stat.against.toString()),
          ],
        ),
      ],
    );
  }
}
